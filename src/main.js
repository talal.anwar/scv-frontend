import Vue from "vue";
import App from "./App.vue";
import router from "./router";
// import googleMapsClient from'@google/maps'
// googleMapsClient.createClient({
//     key: 'AIzaSyDMjvLls40ZPLKSNbanBeezA8pc5WOK75w'
// });


Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");

// window.$ = require('jquery')
// window.JQuery = require('jquery')
var numeral = require('numeral');

/**
 * Formats a phone number.
 * Example: 123-456-7890 => (123) 456-7890
 *
 * @param {String} phone
 * @return {Void}
 */
Vue.filter('phone', function (phone) {
    if(phone && phone !== ''){
        return phone.replace(/[^0-9]/g, '')
            .replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
    }

});